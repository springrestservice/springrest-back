create or replace function set_modify_time() returns trigger as $$
begin
  new.modified_at = now();
  return new;
end;
$$ language plpgsql;
/
alter function set_modify_time() owner to postgres;
/
create table if not exists customers
(
    id          uuid      not null
        constraint customers_pk
            primary key,
    title       varchar(255),
    deleted     boolean,
    created_at  timestamp default CURRENT_TIMESTAMP,
    modified_at timestamp not null
);
/
alter table customers
    owner to postgres;
/
DROP TRIGGER IF EXISTS modify_time_trig on customers;
/
create trigger modify_time_trig
    before insert or update
    on customers
    for each row
execute procedure set_modify_time();
/
create table if not exists products
(
    id          uuid          not null
        constraint products_pk
            primary key,
    customer_id uuid
        constraint customer_id_fk
            references customers,
    title       varchar(255),
    description varchar(1024) not null,
    price       numeric(10, 2),
    deleted     boolean,
    created_at  timestamp default CURRENT_TIMESTAMP,
    modified_at timestamp     not null
);
/
alter table products
    owner to postgres;
/
DROP TRIGGER IF EXISTS modify_time_trig on products;
/
create trigger modify_time_trig
    before insert or update
    on products
    for each row
execute procedure set_modify_time();
/
