package com.springrest.restdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class RestdemoApplication {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(RestdemoApplication.class, args);
	}

	@GetMapping("/test")
	public String test(@RequestParam(value = "val", defaultValue = "default") String val) {
		String cmd = "insert into customers(id, title)\n" +
				"values (uuid_generate_v4(),'"+val+"')";
		int rows = jdbcTemplate.update(cmd);
		return String.format("Rows inserted %s", rows);
	}

}
