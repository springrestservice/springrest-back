package com.springrest.restdemo.db.repo;

import com.springrest.restdemo.db.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface CustomerRepo extends JpaRepository<Customer, UUID> {

    @Query("select c from Customer c where coalesce(c.deleted, false) = :del")
    List<Customer> findCustomersByDeleted(@Param("del") Boolean del);
}
