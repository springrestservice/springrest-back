package com.springrest.restdemo.db.repo;

import com.springrest.restdemo.db.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProductRepo extends JpaRepository<Product, UUID> {
    @Query("select p from Product p where coalesce(p.deleted, false) = :del")
    List<Product> findProductsByDeleted(@Param("del") Boolean del);
}
