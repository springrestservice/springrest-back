package com.springrest.restdemo.db.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne
    private Customer customer;

    private String title;
    private String description;
    private Float price;
    private Boolean deleted;
    private Date created_at;
    private Date modified_at;

    public Product() {

    }

    public UUID getId() {
        return id;
    }

    public Customer getCustomer_id() {
        return customer;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Float getPrice() {
        return price;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public Date getModified_at() {
        return modified_at;
    }

    public void setCreatedDate() {
        if (created_at == null) {
            created_at = new Date();
        }
    }

    public void deleteRecord() {
        deleted = true;
    }

    public void copyData(Product product) {
        this.customer = product.customer;
        this.title = product.title;
        this.description = product.description;
        this.price = product.price;
    }

}
