package com.springrest.restdemo.db.models;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String title;
    private Boolean deleted;
    private Date created_at;
    private Date modified_at;

    public Customer() {

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public Date getModified_at() {
        return modified_at;
    }

    public void setCreatedDate() {
        if (created_at == null) {
            created_at = new Date();
        }
    }

    public void deleteRecord() {
        deleted = true;
    }

}
