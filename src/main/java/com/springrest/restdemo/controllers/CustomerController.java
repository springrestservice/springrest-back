package com.springrest.restdemo.controllers;

import com.google.gson.Gson;
import com.springrest.restdemo.db.models.Customer;
import com.springrest.restdemo.db.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepo customerRepo;

    @GetMapping(value = "/customerlist", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Customer> getCustomers() {
        return customerRepo.findCustomersByDeleted(false);
    }

    @PostMapping(value = "/customers", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public String addCustomer(@RequestBody Customer customer) {
        customer.setCreatedDate();
        customerRepo.save(customer);
        return "OK";
    }

    @DeleteMapping(value = "/customers/{customerId}")
    @Transactional
    public String deleteCustomer(@PathVariable(value="customerId") UUID id) {
        customerRepo.findById(id).ifPresent(Customer::deleteRecord);
        return "OK";
    }

    @GetMapping(value = "/customers/{customerId}")
    public String getCustomer(@PathVariable(value="customerId") UUID id) {
        Customer customer = customerRepo.findById(id).orElse(null);
        Gson gson = new Gson();
        return gson.toJson(customer);
    }

    @PutMapping(value = "/customers/{customerId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String setCustomer(@PathVariable(value="customerId") UUID id, @RequestBody Customer customer) {
        Customer c = customerRepo.findById(id).orElse(null);
        if (c != null) {
            c.setTitle(customer.getTitle());
            customerRepo.save(c);
            return "OK";
        } else {
            return "NOT_FOUND";
        }

    }
}
