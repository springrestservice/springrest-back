package com.springrest.restdemo.controllers;

import com.google.gson.Gson;
import com.springrest.restdemo.db.models.Customer;
import com.springrest.restdemo.db.models.Product;
import com.springrest.restdemo.db.repo.CustomerRepo;
import com.springrest.restdemo.db.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {
    @Autowired
    private ProductRepo productRepo;

    @GetMapping(value = "/productlist", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts() {
        return productRepo.findProductsByDeleted(false);
    }

    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public String addProduct(@RequestBody Product product) {
        product.setCreatedDate();
        productRepo.save(product);
        return "OK";
    }

    @DeleteMapping(value = "/products/{productId}")
    @Transactional
    public String deleteProduct(@PathVariable(value="productId") UUID id) {
        productRepo.findById(id).ifPresent(Product::deleteRecord);
        return "OK";
    }

    @GetMapping(value = "/products/{productId}")
    public String getProduct(@PathVariable(value="productId") UUID id) {
        Product product = productRepo.findById(id).orElse(null);
        Gson gson = new Gson();
        return gson.toJson(product);
    }

    @PutMapping(value = "/products/{productId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String setProduct(@PathVariable(value="productId") UUID id, @RequestBody Product product) {
        Product p = productRepo.findById(id).orElse(null);
        if (p != null) {
            p.copyData(product);
            productRepo.save(p);
            return "OK";
        } else {
            return "NOT_FOUND";
        }

    }
}
